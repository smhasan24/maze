
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class maze {

	static int[] mazeSize = new int[2];
	static int[] mazeStart = new int[2];
	static int[] mazeEnd = new int[2];
	static String[][] maze = null;

	public static void openFile(String fileLocation) {

		/* this method opens the text file passed in from the input argument
		 it then check the first three lines for the maze size, start co-ordinate and end co-ordinate respectively
		 the maze is then generated based on the mazeSize variable
		 some data regarding the maze is then printed as well as the maze it self*/

		int countInputLine = 0;
		int j = 0;

		String line;
		String[] tempLine;


		try {

			File file = new File(fileLocation);
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			while((line = bufferedReader.readLine()) != null) {

				if (countInputLine == 0) { //get the size of the maze
					tempLine = line.split(" ");
					mazeSize[0] = Integer.parseInt(tempLine[0]);
					mazeSize[1] = Integer.parseInt(tempLine[1]);
					maze = new String[mazeSize[1]][mazeSize[0]];
				}else if (countInputLine == 1) { // get the start position
					tempLine = line.split(" ");
					mazeStart[0] = Integer.parseInt(tempLine[0]);
					mazeStart[1] = Integer.parseInt(tempLine[1]);
				}else if (countInputLine == 2) { // get the end position
					tempLine = line.split(" ");
					mazeEnd[0] = Integer.parseInt(tempLine[0]);
					mazeEnd[1] = Integer.parseInt(tempLine[1]);
				}else { // get the maze
					tempLine = line.split(" ");
					for (int i = 0; i < tempLine.length; i++) {
						if (i == 0) {}
						maze[j][i] = tempLine[i];
					}
					j++;
				}
				countInputLine++;
			}
			fileReader.close();
		}catch(IOException e) {
			e.printStackTrace();
		}



		System.out.println("starting at location: " + mazeStart[0] + "," + mazeStart[1]);
		System.out.println("ending at location: " + mazeEnd[0] + "," + mazeEnd[1]);

		// print the maze as is
		for (int y = 0; y < mazeSize[1]; y++) {
			for (int x = 0; x < mazeSize[0]; x++) {
				System.out.print(maze[y][x] + " ");
			}
			System.out.println();
		}
	}

	public static boolean findPath(int x, int y) {

		if (x > mazeSize[0] || y > mazeSize[1]) return false; // you're outside the maze... woops!
		if (x == mazeEnd[0] && y == mazeEnd[1]) return true; // you've reached your goal... yaaay!
		if (!maze[y][x].equals("0")) return false; // this isn't a possible step... try again!

		maze[y][x] = "X"; // we'll mark this position as a possible route

		if (findPath(x+1,y) == true) return true; // try north
		if (findPath(x,y+1) == true) return true; // try east
		if (findPath(x-1,y) == true) return true; // try south
		if (findPath(x,y-1) == true) return true; // try west

		maze[y][x] = " "; // since this location didn't allow us to move we'll clear this location from the maze

		return false;
	}

	public static void main(String[] args) {
		System.out.println("welcome to the maze!");
		//get details from file passed in from input argument
		openFile(args[0]);

		//try find a path
		if (findPath(mazeStart[0],mazeStart[1])) {
			System.out.println("I got to the goal i think :/");
		}else {
			System.out.println("I couldn't find a solution :(");
		}

		maze[mazeStart[1]][mazeStart[0]] = "S"; // marks the maze with the start position
		maze[mazeEnd[1]][mazeEnd[0]] = "E"; // marks the maze with the end position

		// print out the maze in the format requested
		for (int y = 0; y < mazeSize[1]; y++) {
			for (int x = 0; x < mazeSize[0]; x++) {
				if(maze[y][x].equals("1")) maze[y][x] = "#"; // walls are now "#"
				if(maze[y][x].equals("0")) maze[y][x] = " "; // left over open locations are now " "
				System.out.print(maze[y][x]);
			}
			System.out.print("\n");
		}
	}
}
